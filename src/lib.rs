#![feature(libc)]
#![feature(linkage)]
extern crate libc;

#[macro_use]
extern crate lazy_static;

use libc::{c_char, c_int, c_void, RTLD_LOCAL, RTLD_NOW};
use std::ffi::CStr;
use std::mem;
use std::sync::RwLock;

mod controls;
mod indicators;
mod register;

use controls::new_control;
use indicators::new_indicator;

type NiFpgaStatus = u32;

extern "C" {
    fn __libc_dlopen_mode(filename: *const c_char, flag: c_int) -> *mut c_void;
    fn __libc_dlsym(handle: *mut c_void, symbol: *const c_char) -> *mut c_void;
}

unsafe fn real_dlsym(handle: *mut c_void, name: &CStr) -> *mut c_void {
    type DlsymFn = extern "C" fn(handle: *mut c_void, name: *const u8) -> *mut c_void;
    lazy_static! {
        static ref REAL_DLSYM: DlsymFn = unsafe {
            mem::transmute::<*mut c_void, DlsymFn>(__libc_dlsym(
                __libc_dlopen_mode("libdl.so\0".as_ptr(), RTLD_LOCAL | RTLD_NOW),
                "dlsym\0".as_ptr(),
            ))
        };
    }

    REAL_DLSYM(handle, name.as_ptr())
}

type ReadU32Fn = extern "C" fn(session: u32, indicator: u32, value: &mut u32) -> NiFpgaStatus;
lazy_static! {
    static ref real_readU32: RwLock<Option<ReadU32Fn>> = RwLock::new(None);
}

type WriteU32Fn = extern "C" fn(session: u32, control: u32, value: u32) -> NiFpgaStatus;
lazy_static! {
    static ref real_writeU32: RwLock<Option<WriteU32Fn>> = RwLock::new(None);
}

#[no_mangle]
pub unsafe extern "C" fn dlsym(handle: *mut c_void, name_raw: *const u8) -> *const c_void {
    let name = CStr::from_ptr(name_raw);
    println!("{}", name.to_str().unwrap());

    match name.to_bytes() {
        b"dlsym" => dlsym as *const c_void,
        b"NiFpgaDll_ReadU32" => {
            let mut read_u32 = real_readU32.write().unwrap();
            if read_u32.is_none() {
                *read_u32 = Some(mem::transmute::<*mut c_void, ReadU32Fn>(real_dlsym(
                    handle, name,
                )));
            }
            register_read_u32 as *const c_void
        }
        b"NiFpgaDll_WriteU32" => {
            let mut write_u32 = real_writeU32.write().unwrap();
            if write_u32.is_none() {
                *write_u32 = Some(mem::transmute::<*mut c_void, WriteU32Fn>(real_dlsym(
                    handle, name,
                )));
            }
            register_write_u32 as *const c_void
        }
        // Other hax
        _ => real_dlsym(handle, name),
    }
}

extern "C" fn register_read_u32(session: u32, indicator: u32, value: &mut u32) -> NiFpgaStatus {
    let ret = real_readU32.read().unwrap().unwrap()(session, indicator, value);

    let reg = new_indicator(indicator, value);

    print!("Read  ");
    println!("{}", reg);

    //*value = reg.modify();

    ret
}

extern "C" fn register_write_u32(session: u32, control: u32, value: u32) -> NiFpgaStatus {
    let reg = new_control(control, value);

    print!("Write ");
    println!("{}", reg);

    real_writeU32.read().unwrap().unwrap()(session, control, value)
}
