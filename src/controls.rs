use std::fmt;

use register::*;

pub fn new_control(offset: u32, value: u32) -> Box<Register> {
    match offset {
        RegLEDs::OFFSET => Box::new(RegLEDs::new(value)),
        RegWatchdogCommand::OFFSET => Box::new(RegWatchdogCommand::new(value)),
        _ => Box::new(RegUnknown::new(offset, value)),
    }
}

// LEDs Control Register
// =====================

#[derive(Default)]
pub struct RegLEDs {
    value: u32,
    rsl: bool,
    mode: u8,
    comm: u8,
}

impl RegisterInfo for RegLEDs {
    const NAME: &'static str = "LEDs";
    const OFFSET: u32 = 0x18010;
    const SIZE: RegSize = RegSize::U32;
}

impl RegLEDs {
    pub fn new(value: u32) -> Self {
        let mut reg: RegLEDs = Default::default();
        reg.set_value(value);
        reg
    }
}

impl Control for RegLEDs {}
impl Indicator for RegLEDs {}

/*macro_rules control_impl {
    ($($struct_name:ident),*) => {
        $(
            impl Control for $struct_name {}
            )*
    }
}*/

impl Register for RegLEDs {
    fn name(&self) -> &str {
        Self::NAME
    }
    fn offset(&self) -> u32 {
        Self::OFFSET
    }
    fn size(&self) -> RegSize {
        Self::SIZE
    }
    fn set_value(&mut self, value: u32) {
        self.value = value;
        self.rsl = (value & 1) == 1;
        self.mode = ((value >> 1) & 0xff) as u8;
        self.comm = ((value >> 9) & 0xff) as u8;
    }
    fn get_value(&self) -> u32 {
        let mut value = if self.rsl { 1 } else { 0 };
        value |= u32::from(self.mode) << 1;
        value |= u32::from(self.comm) << 9;
        value
    }
    fn modify(&mut self) -> u32 {
        self.rsl = true;
        self.mode = 1;
        self.comm = 1;
        self.get_value()
    }
    fn reg_fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}({:x}) ", RegLEDs::NAME, RegLEDs::OFFSET);
        write!(
            f,
            "Raw: {}. RSL: {}. Mode: {}. Comm: {}",
            self.value, self.rsl, self.mode, self.comm
        )
    }
}

// Watchdog Command Control Register
// =====================

#[derive(Default)]
pub struct RegWatchdogCommand {
    value: u32,
}

impl RegisterInfo for RegWatchdogCommand {
    const NAME: &'static str = "Sys.WatchdogCommand";
    const OFFSET: u32 = 0x1801e;
    const SIZE: RegSize = RegSize::U16;
}

impl RegWatchdogCommand {
    pub fn new(value: u32) -> Self {
        let mut reg: RegWatchdogCommand = Default::default();
        reg.set_value(value);
        reg
    }
}

impl Control for RegWatchdogCommand {}

impl Register for RegWatchdogCommand {
    fn name(&self) -> &str {
        Self::NAME
    }
    fn offset(&self) -> u32 {
        Self::OFFSET
    }
    fn size(&self) -> RegSize {
        Self::SIZE
    }
    fn set_value(&mut self, value: u32) {
        self.value = value;
    }
    fn get_value(&self) -> u32 {
        self.value
    }
    fn reg_fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}({:x}) ", Self::NAME, Self::OFFSET);
        if self.value & 0xff00 == 0xa300 {
            write!(f, "Response: {}", self.value & 0xff)
        } else {
            match self.value {
                0xb007 => write!(f, "Boot"),
                0xdead => write!(f, "Dead"),
                0xfeed => write!(f, "Feed"),
                _ => write!(f, "Unknown: {:4x}", self.value),
            }
        }
    }
}
