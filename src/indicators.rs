use std::fmt;

use controls::RegLEDs;
use register::*;

pub fn new_indicator(offset: u32, value: &mut u32) -> Box<Register> {
    match offset {
        RegLEDs::OFFSET => Box::new(RegLEDs::new(*value)),
        RegPowerVinVoltage::OFFSET => Box::new(RegPowerVinVoltage::new(*value)),
        RegWatchdogStatus::OFFSET => Box::new(RegWatchdogStatus::new(*value)),
        RegWatchdogChallenge::OFFSET => Box::new(RegWatchdogChallenge::new(*value)),
        _ => Box::new(RegUnknown::new(offset, *value)),
    }
}

// Power VinVoltage Indicator Register
// =====================

#[derive(Default)]
pub struct RegPowerVinVoltage {
    value: u32,
    voltage: f32,
}

impl RegisterInfo for RegPowerVinVoltage {
    const NAME: &'static str = "Power.VinVoltage";
    const OFFSET: u32 = 0x18366;
    const SIZE: RegSize = RegSize::U16;
}

impl RegPowerVinVoltage {
    pub fn new(value: u32) -> Self {
        let mut reg: RegPowerVinVoltage = Default::default();
        reg.set_value(value);
        reg
    }
}

impl Indicator for RegPowerVinVoltage {}

impl Register for RegPowerVinVoltage {
    fn name(&self) -> &str {
        Self::NAME
    }
    fn offset(&self) -> u32 {
        Self::OFFSET
    }
    fn size(&self) -> RegSize {
        Self::SIZE
    }
    fn set_value(&mut self, value: u32) {
        self.value = value;
        self.voltage = (value as f32) * 0.006_269;
    }
    fn get_value(&self) -> u32 {
        self.value
    }
    fn reg_fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}({:x}) ", Self::NAME, Self::OFFSET,);
        write!(f, "Voltage: {}", self.voltage)
    }
}

// Watchdog Status Indicator Register
// =====================

#[derive(Default)]
pub struct RegWatchdogStatus {
    value: u32,
    power_disable_count: u16,
    sys_disable_count: u16,
    power_alive: bool,
    system_active: bool,
}

impl RegisterInfo for RegWatchdogStatus {
    const NAME: &'static str = "Sys.WatchdogStatus";
    const OFFSET: u32 = 0x18018;
    const SIZE: RegSize = RegSize::U16;
}

impl RegWatchdogStatus {
    pub fn new(value: u32) -> Self {
        let mut reg: RegWatchdogStatus = Default::default();
        reg.set_value(value);
        reg
    }
}

impl Indicator for RegWatchdogStatus {}

impl Register for RegWatchdogStatus {
    fn name(&self) -> &str {
        Self::NAME
    }
    fn offset(&self) -> u32 {
        Self::OFFSET
    }
    fn size(&self) -> RegSize {
        Self::SIZE
    }
    fn set_value(&mut self, value: u32) {
        self.value = value;
        self.power_disable_count = (value & 0x7fff) as u16;
        self.sys_disable_count = ((value >> 15) & 0x7fff) as u16;
        self.power_alive = (value & 0x4000_0000) > 0;
        self.system_active = (value & 0x8000_0000) > 0;
    }
    fn get_value(&self) -> u32 {
        self.value
    }
    fn reg_fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}({:x}) ", Self::NAME, Self::OFFSET);
        write!(
            f,
            "Active: {}. Power: {}. SysCount: {}. PowerCount: {}",
            self.system_active, self.power_alive, self.sys_disable_count, self.power_disable_count
        )
    }
}

// Watchdog Challenge Indicator Register
// =====================

#[derive(Default)]
pub struct RegWatchdogChallenge {
    value: u32,
}

impl RegisterInfo for RegWatchdogChallenge {
    const NAME: &'static str = "Sys.WatchdogChallenge";
    const OFFSET: u32 = 0x18022;
    const SIZE: RegSize = RegSize::U16;
}

impl RegWatchdogChallenge {
    pub fn new(value: u32) -> Self {
        let mut reg: RegWatchdogChallenge = Default::default();
        reg.set_value(value);
        reg
    }
}

impl Indicator for RegWatchdogChallenge {}

impl Register for RegWatchdogChallenge {
    fn name(&self) -> &str {
        Self::NAME
    }
    fn offset(&self) -> u32 {
        Self::OFFSET
    }
    fn size(&self) -> RegSize {
        Self::SIZE
    }
    fn set_value(&mut self, value: u32) {
        self.value = value;
    }
    fn get_value(&self) -> u32 {
        self.value
    }
    fn reg_fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}({:x}) ", self.name(), self.offset());
        write!(f, "{}", self.value)
    }
}
