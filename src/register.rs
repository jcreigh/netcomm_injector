use std::fmt;

pub enum RegSize {
    U16 = 16,
    U32 = 32,
    Unknown,
}

pub trait Control {}
pub trait Indicator {}
pub trait Modifiable {}

pub trait RegisterInfo {
    const NAME: &'static str;
    const OFFSET: u32;
    const SIZE: RegSize;
}

pub trait Register {
    fn name(&self) -> &str;
    fn offset(&self) -> u32;
    fn size(&self) -> RegSize;
    fn set_value(&mut self, value: u32);
    fn get_value(&self) -> u32;
    fn modify(&mut self) -> u32 {
        self.get_value()
    }
    fn reg_fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "2 {}({:x}): {}",
            self.name(),
            self.offset(),
            self.get_value()
        )
    }
}

impl fmt::Display for dyn Register {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.reg_fmt(f)
    }
}

// Unknown Register
// =====================

pub struct RegUnknown {
    value: u32,
    offset: u32,
}

impl RegisterInfo for RegUnknown {
    const NAME: &'static str = "Unknown";
    const OFFSET: u32 = 0;
    const SIZE: RegSize = RegSize::Unknown;
}

// TODO: Learn macros
impl Register for RegUnknown {
    fn name(&self) -> &str {
        RegUnknown::NAME
    }
    fn offset(&self) -> u32 {
        self.offset
    }
    fn size(&self) -> RegSize {
        RegUnknown::SIZE
    }
    fn set_value(&mut self, value: u32) {
        self.value = value
    }
    fn get_value(&self) -> u32 {
        self.value
    }
    fn reg_fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "3 Unknown({:x}): {}", self.offset, self.value)
    }
}

impl RegUnknown {
    pub fn new(offset: u32, value: u32) -> Self {
        RegUnknown { value, offset }
    }
}
