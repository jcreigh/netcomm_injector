# NetComm Injector

Injects itself in the middle of NetComm FPGA calls so they can be viewed and/or
rewritten.

## Usage

```
% /etc/init.d/FRCNetComm stop; killall FRC_NetCommDaemon
% LD_PRELOAD=libnetcomm_injector.so /usr/local/frc/bin/FRC_NetCommDaemon
```

## License

Licensed under either of

 * Apache License, Version 2.0
   ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license
   ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
